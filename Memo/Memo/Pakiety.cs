﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memo
{
    [Serializable]
    public class GraczKlient
    {
        public String nazwaGracza;
        //public Thread watekGracza;
        //public TcpClient laczeTCP;
        public int nrGracza;
        public String ip;
        public int punkty;
        //public BinaryReader czytanie;
    }


    [Serializable]
    public class Host
    {
        public String nazwaSerwera;            
        public int maxLiczbaGraczy;
        public int poziomTrudnosci;
        public IPAddress adres;
        public int port;
        public String autor;
    }

    [Serializable]
    public struct ProsbaODaneHosta
    {
        public bool status;
    }

 //   class Uzytkownik
  //  {
  //      
  //  }
    
    //[Serializable]
   // public class PaczkaPostepuFake
   // {     
  //      public List<PoleGry> Plansza = new List<PoleGry>();
  //      public int costam;
  //      public Hashtable listaGraczy = new Hashtable();
  //  }


    

    [Serializable]
    public class DaneGry
    {
        public bool start = false;
        public Hashtable listaGraczy = new Hashtable();
        [NonSerialized] public List<PoleGry> Plansza = new List<PoleGry>();
        public Paczka paczka = new Paczka();
        public int width;
        public int height;
        public int rzedow;

        public void wykonajPaczke(Paczka paczka)
        {
            ladujPaczke(paczka);
            logikaGry.aktualizujPlanszeZMapy(paczka.mapa);
            ((GraczKlient)listaGraczy[paczka.nrGraczaKtorySieRuszyl]).punkty += paczka.zdobytePunkt;
            if (paczka.koniecGry)
            {
                GraczKlient graczKlient = maxPunktowGracz();
                MessageBox.Show("KONIEC GRY/n WYGRAL " + graczKlient.nazwaGracza);
            }
        }

        public void testZwyciestwa()
        {
            bool koniec = true;
            foreach (PoleGry poleGry in Plansza)
            {
                if (poleGry.zdobyta)
                {
                    
                }
                else
                {
                    koniec = false;
                }
            }
            if (koniec)
            {
                MessageBox.Show("KONIEC GRY");
            }
        }

        public GraczKlient maxPunktowGracz()
        {
            int max = 0;
            GraczKlient zwyciesca = null;
            foreach(GraczKlient gracz in this.listaGraczy)
            {
                if(gracz.punkty > max) {
                    max = gracz.punkty;
                    zwyciesca = gracz;
                }
            }

            return zwyciesca;
        }

        //public void generujPlanszeZMapy(int rzedow)
        //{
        //    Plansza = new List<PoleGry>();
        //    Plansza.Clear();
        //    int w = 0;
        //    int sz = 0;
        //    int id = 0;
            
        //    foreach (string kod in paczka.mapa)
        //    {
        //        Plansza.Clear();
        //        PoleGry wzor = new PoleGry();
        //        wzor.Image = Memo.Properties.Resources.Revers; 
        //        wzor.Location = new Point(47 + (sz * 155), 78 + (w * 165));
        //        wzor.Margin = new Padding(2);
        //        wzor.Name = kod;
        //        wzor.id = id;
        //        wzor.Size = new Size(150, 162);
        //        wzor.SizeMode = PictureBoxSizeMode.Zoom;
        //        wzor.TabIndex = 33;
        //        wzor.TabStop = false;
        //        wzor.Enabled = false;
        //        wzor.Click += new System.EventHandler(DaneStale.logika.klikPrzycisk);
        //        wzor.X = sz;
        //        wzor.Y = w;
        //        wzor.awersKarty = DaneStale.getObraz(kod);
        //        wzor.odkryta = false;
        //        wzor.zdobyta = false;
        //        Plansza.Add(wzor);
        //        sz++;
        //        if (sz >= rzedow)
        //        {
        //            w++;
        //            sz = 0;
        //        }
        //        id++;
        //    }

        //}

        [NonSerialized] LogikaGry logikaGry;

        public void aktualizujLogikeGry()
        {
            logikaGry.daneGry = this;

        }

        public void generujMapeIUstawLogike(NetworkStream ns)
        {
            Size size = new Size(width, height);
            
            Plansza = new List<PoleGry>();
            logikaGry = new LogikaGry(ns, this);
            Plansza.Clear();
            int w = 0;
            int sz = 0;
            int id = 0;
            
            foreach (string kod in paczka.mapa)
            {             
                PoleGry wzor = new PoleGry();
                
                wzor.Image = Memo.Properties.Resources.Revers;
                wzor.Location = new Point(47 + (sz * width), 78 + (w * height));
                wzor.Margin = new Padding(10);
                wzor.Name = kod;
                wzor.Size = size;
                //wzor.Size = new Size(150, 162);
                wzor.SizeMode = PictureBoxSizeMode.Zoom;
                wzor.TabIndex = 33;
                wzor.id = id;
                wzor.TabStop = false;
                wzor.Enabled = false;
                wzor.Click += new System.EventHandler(logikaGry.klikPrzycisk);
                wzor.X = sz;
                wzor.Y = w;
                wzor.awersKarty = DaneStale.getObraz(kod);
                wzor.odkryta = false;
                wzor.zdobyta = false;
                Plansza.Add(wzor);
                sz++;
                if (sz >= rzedow)
                {
                    w++;
                    sz = 0;
                }
                id++;
            }

           // logikaGry.aktualizuj(this);
        }




        public void zapiszPlanszeDoMapy()
        {
            paczka.mapa.Clear();
            foreach (PoleGry poleGry in Plansza)
            {
                if (poleGry.zdobyta)
                {
                    paczka.mapa.Add(" ");
                }
                else
                {
                    paczka.mapa.Add(poleGry.Name);
                }
            }
        }

        public void ladujMape(List<string> mapa)
        {
            this.paczka.mapa.Clear();
            this.paczka.mapa = mapa;
        }

        public void ladujPaczke(Paczka paczka)
        {
            this.paczka = paczka;
        }



        public Paczka getPaczka()
        {
            zapiszPlanszeDoMapy();
            return paczka;
        }


    }


    [Serializable]
    public class Paczka
    {
        public List<string> mapa = new List<string>();
        public int nrGraczaKtorySieRuszyl;
        public int zdobytePunkt = 0;
        public bool koniecGry;
    }

    [Serializable]
    public class Ruch
    {
        public Ruch(int kolej)
        {
            this.kolej = kolej;
        }
        public int kolej;
    }
    [Serializable]
    public class RezultatRuchu
    {

    }


    [Serializable]
    public struct Komunikat
    {
        public string tresc;
        public string nadawca;
        public int status;
        public DateTime czasWyslania;
        public DateTime czasOdebrania;

    }

    class ZdarzenieWyslaniaKomunikatu: EventArgs
    {
        public Komunikat komunikat;
        public long idPolaczenia;
        public ZdarzenieWyslaniaKomunikatu()
        {
            komunikat.nadawca = "";
            komunikat.tresc = "";
            komunikat.czasOdebrania = DateTime.Now;
        }
    }

}







