﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestAplikacjaOkienkowa;

namespace Memo
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DaneStale.menu.Hide();
            DaneStale.menuWyboru = new MenuWyboru();
            DaneStale.menuWyboru.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DaneStale.zasady = new Zasady();
            DaneStale.zasady.Show();
        }

        private void ButtonWyjdz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Menu_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {        
            Application.Exit();
        }

        private void fontDialog1_Apply(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }

    
}
