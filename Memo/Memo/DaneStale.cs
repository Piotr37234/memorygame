﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestAplikacjaOkienkowa;

namespace Memo
{
    class DaneStale
    {

        public static Hashtable Obrazy = new Hashtable();

        public static void inicjuj()
        {
            Obrazy.Add("R", Properties.Resources.Revers);
            Obrazy.Add("A", Properties.Resources.Ananas);
            Obrazy.Add("AG", Properties.Resources.Agrest);
            Obrazy.Add("AR", Properties.Resources.Arbuz);
            Obrazy.Add("BA", Properties.Resources.Banan);
            Obrazy.Add("B1", Properties.Resources.Borowka);
            Obrazy.Add("B2", Properties.Resources.Borowka2);
            Obrazy.Add("B3", Properties.Resources.Borowka3);
            Obrazy.Add("BR", Properties.Resources.Brzoskwinia);
            Obrazy.Add("C", Properties.Resources.Cytryny);
            Obrazy.Add("D", Properties.Resources.Daktyle);
            Obrazy.Add("G", Properties.Resources.Granat);
            Obrazy.Add("GR", Properties.Resources.Grapefruit);
            Obrazy.Add("GRU", Properties.Resources.Gruszka);
            Obrazy.Add("J", Properties.Resources.Jablko);
            Obrazy.Add("JA", Properties.Resources.Jagody);
            Obrazy.Add("JE", Properties.Resources.Jezyna);
            Obrazy.Add("K", Properties.Resources.Kaki);
            Obrazy.Add("KI", Properties.Resources.Kiwi);
            Obrazy.Add("M", Properties.Resources.Maliny);
            Obrazy.Add("MA", Properties.Resources.Mandarynka);
            Obrazy.Add("MAN", Properties.Resources.Mango);
            Obrazy.Add("ME", Properties.Resources.Melon);
            Obrazy.Add("P", Properties.Resources.Pomarancza);
            Obrazy.Add("P1", Properties.Resources.Porzeczka);
            Obrazy.Add("P2", Properties.Resources.Porzeczka2);
            Obrazy.Add("PO", Properties.Resources.Poziomka);
            Obrazy.Add("S", Properties.Resources.Sliwka);
            Obrazy.Add("T", Properties.Resources.Truskawka);
            Obrazy.Add("W", Properties.Resources.Winogrona);
            Obrazy.Add("WI", Properties.Resources.Wisnie);

        }

        public static Image getObraz(String kod)
        {
            return (Image)Obrazy[kod];
        }

        
        public static MenuWyboru menuWyboru;
        public static OknoStworzNowySerwer oknoStworzNowySerwer;
        public static OknoKlienta oknoKlienta;
        public static OknoSerwera oknoSerwera;
        public static Menu menu;
        public static Serwer serwer;
        public static Zasady zasady;

        public static Hashtable listaWidocznychSerwerow = new Hashtable();

        public ArrayList Plansza = new ArrayList();
        public Hashtable listaGraczy = new Hashtable();

        public static LogikaGry logika = new LogikaGry();


    }
}
