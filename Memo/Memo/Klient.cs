﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memo
{
    public class Klient
    {   
                      
        public Host host;
        public DaneGry daneGry;
        public GraczKlient graczKlient;
        public OknoKlienta oknoKlienta;
        public Thread watekKlienta;
        LogikaGry logikaGry;


        public Klient(String nazwaGracza, OknoKlienta oknoKlienta)
        {
             host = new Host();
             daneGry = new DaneGry();
             daneGry.Plansza = new List<PoleGry>();
            graczKlient = new GraczKlient();
             graczKlient.nazwaGracza = nazwaGracza;
             this.oknoKlienta = oknoKlienta;
             oknoKlienta.klient = this;         
        }

        public Klient()
        {

        }


        public void setKlientData(string nick)
        {
            this.graczKlient.nazwaGracza = nick;
        }


        //public void dolaczEventHandlerDoPlanszyWPaczce()
        //{
        //    DaneStale.logika.dolaczEventHandlerklikPrzycisk(paczkaPostepuGry.Plansza);
        //}

        TcpClient serwer;
        public NetworkStream ns;
        BinaryReader binaryReader;
        BinaryWriter binaryWriter;
        BinaryFormatter bf;

        public void startClient(string nazwaHosta)
        {
            host = getHostZListyWidocznych(nazwaHosta);
            serwer = new TcpClient(host.adres+"", host.port);
            ns = serwer.GetStream();
            binaryReader = new BinaryReader(ns);
            binaryWriter = new BinaryWriter(ns);
            bf = new BinaryFormatter();
            binaryWriter.Write("JESTEM GRACZEM I CHCE SIE DOLACZYC");
            bf.Serialize(ns, graczKlient);
            daneGry = (DaneGry)bf.Deserialize(ns);
            foreach(DictionaryEntry gracz in daneGry.listaGraczy)
            {
               String nazwa = ((GraczKlient)gracz.Value).nazwaGracza;
                if (nazwa == graczKlient.nazwaGracza)
                {
                    graczKlient = (GraczKlient)gracz.Value;
                }
            }
            daneGry.generujMapeIUstawLogike(ns);
            //MessageBox.Show("Odebralem dane od serwera");
            //dolaczEventHandlerDoPlanszyWPaczce();
            oknoKlienta.Show();


            //tu sie uruchamia worker
            watekKlienta = new Thread(new ThreadStart(SluchaczKliecki));
            watekKlienta.Name = "Klient 1";
            watekKlienta.IsBackground = false;
            watekKlienta.Start();
            //MessageBox.Show("Worker: Uruchamiam sluchacz");

        }
        public void SluchaczKliecki()
        {
            while (true)
            {
                Ruch ruch = (Ruch)bf.Deserialize(ns);
               // MessageBox.Show("ODEBRALEM RUCH OD SERWERA");
                if (ruch.kolej == graczKlient.nrGracza)
                {
                    oknoKlienta.OdswierzListeGraczy();
                    MessageBox.Show("TWOJ RUCH"+graczKlient.nazwaGracza);
                    aktywacjaBuutonow();
                }               
                Paczka paczka = (Paczka)bf.Deserialize(ns);
              //  MessageBox.Show("ODEBRALEM PACZKE OD SERWERA");
                daneGry.wykonajPaczke(paczka);
            }
        }

        public void dezaktywacjaBuutonow()
        {
            foreach (PoleGry foto in daneGry.Plansza)
            {
                foto.Enabled = false;

            }

        }


        public void aktywacjaBuutonow()
        {
            foreach (PoleGry foto in daneGry.Plansza)
            {
                foto.Enabled = true;

            }

        }



        public Host getHostZListyWidocznych(string nazwa)
        {
            foreach (DictionaryEntry host in DaneStale.listaWidocznychSerwerow)
            {
                string nazwaSerwera = ((Host)host.Value).nazwaSerwera;
                if(nazwaSerwera == nazwa)
                {
                    return (Host)host.Value;
                }
                
            }
            MessageBox.Show("Nie znalezionow hosta o podanej nazwie! ");
            return null;
        }



















        public void FiltrujAdresyIP(List<IPInfo> lista)
        {
            bool dalej = false;

            do
            {
                foreach (IPInfo i in lista)
                {
                    // za musze isc bede za jakas godzike 
                    String[] tab = i.IPAddress.Split('.');                    
                    int pierwszy = int.Parse(tab[0]);
                    int czwarty = int.Parse(tab[3]);
                    if (pierwszy >= 224 || czwarty == 255 || czwarty == 0)
                    {
                        lista.Remove(i);
                        dalej = true;
                        break;
                    }
                    else
                    {
                        dalej = false;
                    }

                }               
            } while (dalej);

        }



        public void uaktualnijListeSerwerow()
        {

            List<IPInfo> lista = IPInfo.GetIPInfo();
            FiltrujAdresyIP(lista);
            String kom ="" ;
            foreach (IPInfo i in lista)
            {
                kom += "   "+i.IPAddress+"\n";
            }
            
            int nr = 0;
            DaneStale.listaWidocznychSerwerow.Clear();
            //MessageBox.Show(kom);
            //String test = "";
            //foreach (IPEndPoint t in IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpListeners())
            //{
            //    test += "   " + t.Address + "\n";
            //}
            //MessageBox.Show(test);
            //test = "";
            //foreach (TcpConnectionInformation t in IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpConnections())
            //{
            //    test += "   " + t.LocalEndPoint + " " +t.LocalEndPoint + "\n";
            //}
            //MessageBox.Show(test);




            /*essageBox.Show(kom);
*/
            //foreach (IPEndPoint t in  IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpListeners()) {

            lista.Add(new IPInfo("moj", "25.90.44.183"));
             foreach (IPInfo iPInfo in lista)
            { 
                
                IPEndPoint t = new IPEndPoint(IPAddress.Parse(iPInfo.IPAddress), 2222);
               // IPEndPoint t = new IPEndPoint(IPAddress.Parse("25.90.44.183"), 2222);
                byte[] bufor = new byte[100];
                PingOptions pingOptions = new PingOptions();
                try
                {
                    Ping ping = new Ping();
                    PingReply odp = ping.Send(t.Address, 3, bufor, pingOptions);
                    if (odp.Status == IPStatus.Success)
                    {
                     // MessageBox.Show(" Ping Sukces " + t.Address.ToString() + " " + t.Port);
                        //MessageBox.Show(t.Address.ToString() + ":" + t.Port.ToString());
                        TcpClient serwer = new TcpClient(t.Address.ToString(), t.Port);
                        NetworkStream ns = serwer.GetStream();
                        BinaryReader binaryReader = new BinaryReader(ns);
                        BinaryWriter binaryWriter = new BinaryWriter(ns);
                        BinaryFormatter bf = new BinaryFormatter();
                        binaryWriter.Write("CHCE INFORMACJI OD CIEBIE");
                        Host host = (Host)bf.Deserialize(ns);
                        DaneStale.listaWidocznychSerwerow.Add(nr, host);
                        nr++;
                       // MessageBox.Show(" Sukces " + t.Address.ToString() + " " + t.Port);
                        //break;
                    }
                    //}
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(" Niepowodzenie " + t.Address.ToString() + " " + t.Port);
                }

            }
           }
        






        private BinaryFormatter formatowacz = new BinaryFormatter();  













        public  void aktulizacjaTablicyZSerwera()
        {
            short port = 2222;
           string Ipadres = "169.254.113.128";
            //ring host = textBox1.Text;
            //istBox1.Items.Add("Skanowanie portów dla " + host);
            //try
           // {
                 TcpClient serwer = new TcpClient(Ipadres, port);
            //    TcpClient serwergniazdo =  DaneStale.serwer.startKlient(Ipadres, port);
                //Okna.form1.setTextTest("dziala :) ");
             //    Komunikat kom = new Komunikat();
              //   kom.status = 2;                                                                                                      
             ///    formatowacz.Serialize(serwergniazdo.GetStream(), kom);                                                                                                                
             //    Host odebraneInfoOHoscie = (Host)formatowacz.Deserialize(serwergniazdo.GetStream());                                                                     
              //    while (Serwer.listaSerwerow.Contains(Serwer.IdPolaczenia))
              //   {
              //       Interlocked.Increment(ref Serwer.IdPolaczenia);
             //    }                
               //   Serwer.listaSerwerow.Add(Serwer.IdPolaczenia, odebraneInfoOHoscie);
        }
    }
}
