﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Memo
{
    class Start
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            DaneStale.inicjuj();
            DaneStale.menuWyboru = new TestAplikacjaOkienkowa.MenuWyboru();
            
            DaneStale.serwer = new Serwer();
            DaneStale.oknoStworzNowySerwer = new TestAplikacjaOkienkowa.OknoStworzNowySerwer(null);
            //DaneStale.oknoSerwera = new OknoSerwera();
            //DaneStale.oknoKlienta = new OknoKlienta();
            DaneStale.menu = new Menu();
           
            //DaneStale.oknoKlienta = new OknoKlienta();
            Application.Run(DaneStale.menu);
        }

        static void Koniec()
        {
            Application.Exit();
        }
    }
}
