﻿namespace Memo
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ButtonRozpocznijGre = new System.Windows.Forms.Button();
            this.ButtonZasadyGry = new System.Windows.Forms.Button();
            this.ButtonWyjdz = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(29, 13);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(983, 513);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // ButtonRozpocznijGre
            // 
            this.ButtonRozpocznijGre.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ButtonRozpocznijGre.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ButtonRozpocznijGre.Location = new System.Drawing.Point(29, 562);
            this.ButtonRozpocznijGre.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonRozpocznijGre.Name = "ButtonRozpocznijGre";
            this.ButtonRozpocznijGre.Size = new System.Drawing.Size(288, 58);
            this.ButtonRozpocznijGre.TabIndex = 1;
            this.ButtonRozpocznijGre.Text = "Rozpocznij Gre";
            this.ButtonRozpocznijGre.UseVisualStyleBackColor = false;
            this.ButtonRozpocznijGre.Click += new System.EventHandler(this.button1_Click);
            // 
            // ButtonZasadyGry
            // 
            this.ButtonZasadyGry.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ButtonZasadyGry.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ButtonZasadyGry.Location = new System.Drawing.Point(379, 562);
            this.ButtonZasadyGry.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonZasadyGry.Name = "ButtonZasadyGry";
            this.ButtonZasadyGry.Size = new System.Drawing.Size(288, 58);
            this.ButtonZasadyGry.TabIndex = 2;
            this.ButtonZasadyGry.Text = "Zasady Gry";
            this.ButtonZasadyGry.UseVisualStyleBackColor = false;
            this.ButtonZasadyGry.Click += new System.EventHandler(this.button2_Click);
            // 
            // ButtonWyjdz
            // 
            this.ButtonWyjdz.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ButtonWyjdz.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ButtonWyjdz.Location = new System.Drawing.Point(724, 562);
            this.ButtonWyjdz.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonWyjdz.Name = "ButtonWyjdz";
            this.ButtonWyjdz.Size = new System.Drawing.Size(288, 58);
            this.ButtonWyjdz.TabIndex = 3;
            this.ButtonWyjdz.Text = "Wyjdz";
            this.ButtonWyjdz.UseVisualStyleBackColor = false;
            this.ButtonWyjdz.Click += new System.EventHandler(this.ButtonWyjdz_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label1.Location = new System.Drawing.Point(559, 135);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(395, 135);
            this.label1.TabIndex = 4;
            this.label1.Text = "Memo";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1035, 649);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonWyjdz);
            this.Controls.Add(this.ButtonZasadyGry);
            this.Controls.Add(this.ButtonRozpocznijGre);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.Menu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button ButtonRozpocznijGre;
        private System.Windows.Forms.Button ButtonZasadyGry;
        private System.Windows.Forms.Button ButtonWyjdz;
        private System.Windows.Forms.Label label1;
    }
}