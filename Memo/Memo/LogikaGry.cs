﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memo
{
    class LogikaGry
    {
        public DaneGry daneGry;
        public NetworkStream ns;
        public BinaryFormatter bf = new BinaryFormatter();
        public PoleGry PoleGryPrzechowywaneWPamieci = null;


        public LogikaGry(NetworkStream ns, DaneGry daneGry)
        {
            this.ns = ns;
            this.daneGry = daneGry;
        }

        public LogikaGry()
        {       
        }


        public void aktualizuj(DaneGry daneGry)
        {
            this.daneGry = daneGry;
        }
        //FUNKCJA PRZYCISKI PLANSZY


        //public PaczkaPostepuGry zamienLightNaDuzaPaczke(PaczkaPostepuGryWersjaLight paczkaPostepuGryWersjaLight)
        //{
        //    PaczkaPostepuGry paczkaPostepuGry = new PaczkaPostepuGry();



        //}


        public void odkryj(PoleGry poleGry)
        {
            poleGry.odkryta = true;
            poleGry.Image = poleGry.awersKarty;
        }

        public void zakryj(PoleGry poleGry)
        {
            poleGry.odkryta = false;
            poleGry.Image = DaneStale.getObraz("R");
        }



        public void aktualizujPlanszeZMapy(List<string> mapa)
        {
           // MessageBox.Show(mapa.Count + " :mapa Count");
           // MessageBox.Show(daneGry.Plansza.Count + " :licznik PLansza");


            for (int i = 0; i < mapa.Count; i++)
            {
                 if(mapa[i] == " ")
                 {
                      usunPole(daneGry.Plansza[i]);
                 }
                // else {
                //MessageBox.Show(mapa.Count + " :licznik");
                //MessageBox.Show(i + " :i");
                //MessageBox.Show(daneGry.Plansza[i].id+" :id");
                    daneGry.Plansza[i].awersKarty = DaneStale.getObraz(mapa[i]);
              //  }
               
                
            }
        }


        public void rysujPlansze(DaneGry daneGry,Form okno)
        {
            foreach (PoleGry wzor in daneGry.Plansza)
            {
                okno.Controls.Add(wzor);
            }
        }


        //public void generujPlanszeZMapy(ArrayList mapa, int rzedow,DaneGry daneGry)
        //{
        //    daneGry.Plansza.Clear();
        //    int w = 0;
        //    int sz = 0;
        //    foreach (string kod in mapa)
        //    {
        //        PoleGry wzor = new PoleGry();
        //        wzor.Image = Memo.Properties.Resources.Revers;
        //        wzor.Location = new Point(47 + (sz * 155), 78 + (w * 165));
        //        wzor.Margin = new Padding(2);
        //        wzor.Name = "wzor";
        //        wzor.Size = new Size(150, 162);
        //        wzor.SizeMode = PictureBoxSizeMode.Zoom;
        //        wzor.TabIndex = 33;
        //        wzor.TabStop = false;
        //        wzor.Click += new System.EventHandler(DaneStale.logika.klikPrzycisk);
        //        wzor.X = sz;
        //        wzor.Y = w;
        //        wzor.awersKarty = DaneStale.getObraz(kod);
        //        wzor.odkryta = false;
        //        wzor.zdobyta = false;
        //        daneGry.Plansza.Add(wzor);
        //        sz++;
        //        if (sz >= rzedow)
        //        {
        //            w++;
        //            sz = 0;
        //        }
        //    }

        //}



        public void zapiszPoleDoPamieci(PoleGry poleGry)
        {
            PoleGryPrzechowywaneWPamieci = poleGry;
        }

        public void zwolnijPolePrzechowywaneWPamieci()
        {
            if (PoleGryPrzechowywaneWPamieci != null)
            {
                zakryj(PoleGryPrzechowywaneWPamieci);
                PoleGryPrzechowywaneWPamieci = null;
            }
        }


        public bool czyJestPolePrzchowywaneWPamieci()
        {
            if (PoleGryPrzechowywaneWPamieci == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool czyTakieSameJakPolePrzechowywaneWPamieci(PoleGry poleGry)
        {
            if (PoleGryPrzechowywaneWPamieci == null)
            {
                //MessageBox.Show("Sprawdzenie niemozliwe Odkrte Pole jest nullem");
                return false;
            }

            if (PoleGryPrzechowywaneWPamieci.id == poleGry.id)
            {
               // MessageBox.Show("Sprawdzenie niemozliwe Odkrte Pole jest nullem");
                return false;
            }

            else if (poleGry.awersKarty == PoleGryPrzechowywaneWPamieci.awersKarty)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        public void usunPoleZPamieci()
        {
            PoleGryPrzechowywaneWPamieci.zdobyta = true;
            //PoleGryPrzechowywaneWPamieci.Hide();
            PoleGryPrzechowywaneWPamieci.Image = null;
            PoleGryPrzechowywaneWPamieci = null;
        }

        public void usunPole(PoleGry pole)
        {
            pole.zdobyta = true;
            //pole.Hide();
            pole.Image = null;
            daneGry.paczka.mapa[pole.id] = " ";         
        }

        public void dolaczEventHandlerklikPrzycisk(ArrayList lista)
        {
            foreach (PoleGry wzor in lista)
            {
               wzor.Click += new EventHandler(this.klikPrzycisk);
            }
        }


        public void dezaktywacjaBuutonow()
        {
            foreach(PoleGry foto in daneGry.Plansza)
            {
                foto.Enabled = false;
             
            }

        }


        public bool testZwyciestwa()
        {
            bool koniec = true;
            foreach (PoleGry poleGry in daneGry.Plansza)
            {
                if (poleGry.zdobyta)
                {

                }
                else
                {
                    koniec = false;
                }
            }
            return koniec;
        }

        public void aktywacjaBuutonow()
        {
            foreach (PoleGry foto in daneGry.Plansza)
            {
                foto.Enabled = true;

            }

        }


        public void klikPrzycisk(object sender, EventArgs e)
        {
            PoleGry pole = sender as PoleGry;

            if(pole.zdobyta)
            {
                return;
            }

            if (czyJestPolePrzchowywaneWPamieci())
            {
                odkryj(pole);
                if (czyTakieSameJakPolePrzechowywaneWPamieci(pole))
                {
           
                    usunPoleZPamieci();
                    usunPole(pole);
                    daneGry.zapiszPlanszeDoMapy();
                    daneGry.paczka.zdobytePunkt++;                 
                }
                else
                {
                
                    zwolnijPolePrzechowywaneWPamieci();
                    zakryj(pole);
                    daneGry.zapiszPlanszeDoMapy();
                    dezaktywacjaBuutonow();
                    if (testZwyciestwa())
                    {
                        daneGry.paczka.koniecGry = true;
                  
                    }
                    bf.Serialize(ns, daneGry.paczka);
                }

            }
            else
            {
                odkryj(pole);
                zapiszPoleDoPamieci(pole);
            }

        }

    }
}
