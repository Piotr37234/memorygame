﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memo
{
    [Serializable]
    public class PoleGry : PictureBox, ISerializable
    {
        public int id;
        public int X;
        public int Y;
        public Image awersKarty;
        public bool odkryta = false;
        public bool zdobyta = false;


        public PoleGry()
        {

        }


        public PoleGry(SerializationInfo info, StreamingContext context)
        {
            X = (int)info.GetValue("X", typeof(int));
            Y = (int)info.GetValue("Y", typeof(int));
            id = (int)info.GetValue("id",typeof(int));
            awersKarty = (Image)info.GetValue("awersKarty", typeof(Image));
            odkryta = (bool)info.GetValue("odkryta", typeof(bool));
            zdobyta = (bool)info.GetValue("zdobyta", typeof(bool));
            Name = (string)info.GetValue("Name", typeof(string));
            Image = (Image)info.GetValue("Image", typeof(Image));
            Location = (Point)info.GetValue("Location", typeof(Point));
            Margin = (Padding)info.GetValue("Margin", typeof(Padding));
            Size = (Size)info.GetValue("Size", typeof(Size));
            SizeMode = (PictureBoxSizeMode)info.GetValue("SizeMode", typeof(PictureBoxSizeMode));
            TabIndex = (int)info.GetValue("TabIndex", typeof(int));
            TabStop = (bool)info.GetValue("TabStop", typeof(bool));
            //Click += (EventHandler)info.GetValue("Click", typeof(EventHandler));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("X", X);
            info.AddValue("Y", Y);
            info.AddValue("id", id);
            info.AddValue("awersKarty", awersKarty);
            info.AddValue("odkryta", odkryta);
            info.AddValue("zdobyta", zdobyta);
            info.AddValue("Name", Name);
            info.AddValue("Image", Image);

            info.AddValue("Location", Location);
            info.AddValue("Margin", Margin);
            info.AddValue("Size", Size);
            info.AddValue("SizeMode", SizeMode);
            info.AddValue("TabIndex", TabIndex);
            info.AddValue("TabStop", TabStop);
            //info.AddValue("Click", new EventHandler(DaneStale.logika.klikPrzycisk));
            //   info.AddValue("Click", Click += new System.EventHandler(OknoKlienta.klikPrzycisk));



        }
    }
}
       //                i+=1
 //   i=1+1
               // wzor.Click = wzor.Click + new System.EventHandler(this.klikPrzycisk);

