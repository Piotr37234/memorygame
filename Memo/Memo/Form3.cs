﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Memo;

namespace TestAplikacjaOkienkowa
{
   
    public partial class Form3 : Form
    {
        
        public Form3()
        {
            InitializeComponent();
        }
        


        private void Form3_Load(object sender, EventArgs e)
        {
            HostNazwaPole.Text = "Nazwa Hosta: "+ Serwer.getHost().nazwaSerwera;
            AdresIPPole.Text = "Adres Ip: " + Serwer.getHost().adres;

           ListaUzytkownikow.Items.Add(Serwer.getHost().zalozyciel.nazwaGracza);


            foreach (DictionaryEntry gracz in Serwer.listaGraczy)
            {
                string[] row = { ( (GraczKlient)gracz.Value).nazwaGracza} ;
;               var listViewItem = new ListViewItem(row);
                ListaUzytkownikow.Items.Add(listViewItem);
            }

        }

        public void Odswierz()
        {
            ListaUzytkownikow.Clear();
            ListaUzytkownikow.Items.Add(Serwer.getHost().zalozyciel.nazwaGracza);
            foreach (DictionaryEntry gracz in Serwer.listaGraczy)
            {
                ListaUzytkownikow.Items.Add(((GraczKlient)gracz.Value).nazwaGracza);
            }

        }


        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
        
    }
}
