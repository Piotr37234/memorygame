﻿namespace TestAplikacjaOkienkowa
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.NazwaSerweraPole = new System.Windows.Forms.TextBox();
            this.MaxLiczbaGraczyPole = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PoziomTrudnosciRozwijanaLista = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(101, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(314, 55);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nowy serwer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(96, 186);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Liczba graczy:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(296, 287);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 46);
            this.button1.TabIndex = 2;
            this.button1.Text = "Utwórz";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(98, 140);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 24);
            this.label4.TabIndex = 5;
            this.label4.Text = "Nazwa:";
            // 
            // NazwaSerweraPole
            // 
            this.NazwaSerweraPole.Location = new System.Drawing.Point(268, 140);
            this.NazwaSerweraPole.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.NazwaSerweraPole.Multiline = true;
            this.NazwaSerweraPole.Name = "NazwaSerweraPole";
            this.NazwaSerweraPole.Size = new System.Drawing.Size(90, 24);
            this.NazwaSerweraPole.TabIndex = 6;
            // 
            // MaxLiczbaGraczyPole
            // 
            this.MaxLiczbaGraczyPole.Location = new System.Drawing.Point(268, 186);
            this.MaxLiczbaGraczyPole.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaxLiczbaGraczyPole.Multiline = true;
            this.MaxLiczbaGraczyPole.Name = "MaxLiczbaGraczyPole";
            this.MaxLiczbaGraczyPole.Size = new System.Drawing.Size(90, 24);
            this.MaxLiczbaGraczyPole.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(96, 236);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "Poziom trudności:";
            // 
            // PoziomTrudnosciRozwijanaLista
            // 
            this.PoziomTrudnosciRozwijanaLista.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PoziomTrudnosciRozwijanaLista.FormattingEnabled = true;
            this.PoziomTrudnosciRozwijanaLista.ItemHeight = 22;
            this.PoziomTrudnosciRozwijanaLista.Items.AddRange(new object[] {
            "łatwy",
            "średni",
            "trudny"});
            this.PoziomTrudnosciRozwijanaLista.Location = new System.Drawing.Point(268, 232);
            this.PoziomTrudnosciRozwijanaLista.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.PoziomTrudnosciRozwijanaLista.Name = "PoziomTrudnosciRozwijanaLista";
            this.PoziomTrudnosciRozwijanaLista.Size = new System.Drawing.Size(90, 26);
            this.PoziomTrudnosciRozwijanaLista.TabIndex = 10;
            this.PoziomTrudnosciRozwijanaLista.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 355);
            this.Controls.Add(this.PoziomTrudnosciRozwijanaLista);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.MaxLiczbaGraczyPole);
            this.Controls.Add(this.NazwaSerweraPole);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form2";
            this.Text = "Memo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox NazwaSerweraPole;
        private System.Windows.Forms.TextBox MaxLiczbaGraczyPole;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox PoziomTrudnosciRozwijanaLista;
    }
}