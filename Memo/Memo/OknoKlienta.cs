﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memo
{
    public partial class OknoKlienta : Form
    {

        //  Host host;
        //  PaczkaPostepuGry paczkaPostepuGry;

        public Klient klient;
        
        // public void setKlient(Klient klient)
        //  {
        //      this.klient = klient;
        //  }


        //  public void setAtributes(Host host, PaczkaPostepuGry paczkaPostepuGry)
        // {
        //      this.host = host;
        //      this.paczkaPostepuGry = paczkaPostepuGry;           
        //  }

        //  public void setPaczkaPostepuGry(PaczkaPostepuGry paczkaPostepuGry)
        //   {
        //       this.paczkaPostepuGry = paczkaPostepuGry;
        //   }

        public void OdswierzListeGraczy()
        {
            ListaUzytkownikow.Clear();       
            foreach (DictionaryEntry gracz in klient.daneGry.listaGraczy)
            {
                ListaUzytkownikow.Items.Add(((GraczKlient)gracz.Value).nazwaGracza+" PKTY: "+ ((GraczKlient)gracz.Value).punkty);
            }
        }

        PictureBox wzor = new PictureBox();




        //public void rysujPlansze()
        //{
        //    foreach (PoleGry wzor in klient.daneGry.Plansza)
        //    {
        //        if (wzor.odkryta)
        //        {
        //            wzor.Image = wzor.awersKarty;
        //        }              
        //        this.Controls.Add(wzor);
        //    }
        //}

        public OknoKlienta()
        {
            //MessageBox.Show("Teraz uruchamiam INICJALIZACJE");
            InitializeComponent();
           
        }

        //public PoleGry PoleGryPrzechowywaneWPamieci = null;


        ////FUNKCJA PRZYCISKI PLANSZY


        //private void odkryj(PoleGry poleGry)
        //{
        //    poleGry.odkryta = true;
        //    poleGry.Image = poleGry.awersKarty;
        //}

        //private void zakryj(PoleGry poleGry)
        //{
        //    poleGry.odkryta = false;
        //    poleGry.Image = DaneStale.getObraz("R");
        //}

        //private void zapiszPoleDoPamieci(PoleGry poleGry)
        //{
        //    PoleGryPrzechowywaneWPamieci = poleGry;
        //}

        //private void zwolnijPolePrzechowywaneWPamieci()
        //{
        //    if (PoleGryPrzechowywaneWPamieci != null)
        //    {
        //        zakryj(PoleGryPrzechowywaneWPamieci);
        //        PoleGryPrzechowywaneWPamieci = null;
        //    }
        //}


        //private bool czyJestPolePrzchowywaneWPamieci()
        //{
        //    if (PoleGryPrzechowywaneWPamieci == null)
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}

        //private bool czyTakieSameJakPolePrzechowywaneWPamieci(PoleGry poleGry)
        //{
        //    if (PoleGryPrzechowywaneWPamieci == null)
        //    {
        //        MessageBox.Show("Sprawdzenie niemozliwe Odkrte Pole jest nullem");
        //        return false;
        //    }

        //    else if (poleGry.awersKarty == PoleGryPrzechowywaneWPamieci.awersKarty)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }

        //}


        //private void usunPoleZPamieci()
        //{
        //    PoleGryPrzechowywaneWPamieci.zdobyta = true;
        //    //PoleGryPrzechowywaneWPamieci.Hide();
        //    PoleGryPrzechowywaneWPamieci.Image = null;
        //    PoleGryPrzechowywaneWPamieci = null;
        //}

        //private void usunPole(PoleGry pole)
        //{
        //    pole.zdobyta = true;
        //    //pole.Hide();
        //    pole.Image = null;
        //}


        //int i = 0;
        //private void klikPrzycisk(object sender, EventArgs e)
        //{
        //    PoleGry pole = sender as PoleGry;

        //    //pole.Image = null;

        //    //if (true)
        //    //{
        //    //    return;
        //    //}

        //    if (czyJestPolePrzchowywaneWPamieci())
        //    {
        //        odkryj(pole);
        //        if (czyTakieSameJakPolePrzechowywaneWPamieci(pole))
        //        {
        //            MessageBox.Show("Dostaje punkty gracz");
        //            usunPoleZPamieci();
        //            usunPole(pole);
        //        }
        //        else
        //        {
        //            MessageBox.Show("Bledne dopasowanie! Kolejka na 2 gracza");
        //            /// tu program 
        //            zwolnijPolePrzechowywaneWPamieci();
        //            zakryj(pole);
        //        }

        //    }
        //    else
        //    {
        //        odkryj(pole);
        //        zapiszPoleDoPamieci(pole);
        //    }

        //}




        private void OknoKlienta_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("Teraz uruchamiam LOAD");
            HostNazwaPole.Text = "Nazwa Hosta: " + klient.host.nazwaSerwera;
            AdresIPPole.Text = "Adres Ip: " + klient.host.adres;
            imie.Text = "Imie: "+klient.graczKlient.nazwaGracza;


            OdswierzListeGraczy();
            //MessageBox.Show(klient.daneGry.Plansza[1].X+"");
            DaneStale.logika.rysujPlansze(klient.daneGry,this);
        }


        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void WrocDoMenu()
        {
            this.Close();
            DaneStale.menu.Show();        
        }

        private void button3_Click(object sender, EventArgs e)
        {
            WrocDoMenu();
        }

        private void Test2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
