﻿namespace Memo5
{
    partial class Form11
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAdres = new System.Windows.Forms.TextBox();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.buttonPolacz = new System.Windows.Forms.Button();
            this.buttonSerwuj = new System.Windows.Forms.Button();
            this.richTextBoxOdbior = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonWyslij = new System.Windows.Forms.Button();
            this.textBoxKomunikat = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Wpisz adres IP";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Wpisz port";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // textBoxAdres
            // 
            this.textBoxAdres.Location = new System.Drawing.Point(133, 36);
            this.textBoxAdres.Name = "textBoxAdres";
            this.textBoxAdres.Size = new System.Drawing.Size(100, 20);
            this.textBoxAdres.TabIndex = 2;
            this.textBoxAdres.Text = "127.0.0.1";
            this.textBoxAdres.TextChanged += new System.EventHandler(this.textBoxAdres_TextChanged);
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(133, 68);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(100, 20);
            this.textBoxPort.TabIndex = 3;
            this.textBoxPort.Text = "2222";
            this.textBoxPort.TextChanged += new System.EventHandler(this.textBoxPort_TextChanged);
            // 
            // buttonPolacz
            // 
            this.buttonPolacz.Location = new System.Drawing.Point(214, 139);
            this.buttonPolacz.Name = "buttonPolacz";
            this.buttonPolacz.Size = new System.Drawing.Size(125, 39);
            this.buttonPolacz.TabIndex = 4;
            this.buttonPolacz.Text = "Polacz się z serwerem";
            this.buttonPolacz.UseVisualStyleBackColor = true;
            this.buttonPolacz.Click += new System.EventHandler(this.buttonPolacz_Click);
            // 
            // buttonSerwuj
            // 
            this.buttonSerwuj.Location = new System.Drawing.Point(74, 139);
            this.buttonSerwuj.Name = "buttonSerwuj";
            this.buttonSerwuj.Size = new System.Drawing.Size(104, 39);
            this.buttonSerwuj.TabIndex = 5;
            this.buttonSerwuj.Text = "Utworz serwer";
            this.buttonSerwuj.UseVisualStyleBackColor = true;
            this.buttonSerwuj.Click += new System.EventHandler(this.buttonSerwuj_Click);
            // 
            // richTextBoxOdbior
            // 
            this.richTextBoxOdbior.Location = new System.Drawing.Point(74, 204);
            this.richTextBoxOdbior.Name = "richTextBoxOdbior";
            this.richTextBoxOdbior.Size = new System.Drawing.Size(278, 94);
            this.richTextBoxOdbior.TabIndex = 6;
            this.richTextBoxOdbior.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Czat";
            // 
            // buttonWyslij
            // 
            this.buttonWyslij.Location = new System.Drawing.Point(195, 361);
            this.buttonWyslij.Name = "buttonWyslij";
            this.buttonWyslij.Size = new System.Drawing.Size(75, 23);
            this.buttonWyslij.TabIndex = 8;
            this.buttonWyslij.Text = "Wyslij";
            this.buttonWyslij.UseVisualStyleBackColor = true;
            this.buttonWyslij.Click += new System.EventHandler(this.buttonWyslij_Click);
            // 
            // textBoxKomunikat
            // 
            this.textBoxKomunikat.Location = new System.Drawing.Point(74, 320);
            this.textBoxKomunikat.Name = "textBoxKomunikat";
            this.textBoxKomunikat.Size = new System.Drawing.Size(274, 20);
            this.textBoxKomunikat.TabIndex = 9;
            // 
            // Form11
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 396);
            this.Controls.Add(this.textBoxKomunikat);
            this.Controls.Add(this.buttonWyslij);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.richTextBoxOdbior);
            this.Controls.Add(this.buttonSerwuj);
            this.Controls.Add(this.buttonPolacz);
            this.Controls.Add(this.textBoxPort);
            this.Controls.Add(this.textBoxAdres);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form11";
            this.Text = "Memo";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form11_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxAdres;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Button buttonPolacz;
        private System.Windows.Forms.Button buttonSerwuj;
        private System.Windows.Forms.RichTextBox richTextBoxOdbior;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonWyslij;
        private System.Windows.Forms.TextBox textBoxKomunikat;
    }
}

