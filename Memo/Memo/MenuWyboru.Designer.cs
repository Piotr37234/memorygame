﻿namespace TestAplikacjaOkienkowa
{
    partial class MenuWyboru
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.listaSerwerowLista = new System.Windows.Forms.ListView();
            this.ServerName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PoziomTrudnosci = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.LiczbaGraczy = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.NazwaGracza = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(32, 220);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(266, 70);
            this.button1.TabIndex = 0;
            this.button1.Text = "Utwórz nowy serwer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2.Location = new System.Drawing.Point(32, 323);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(266, 82);
            this.button2.TabIndex = 1;
            this.button2.Text = "Odśwież listę dostępnych serwerów";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // listaSerwerowLista
            // 
            this.listaSerwerowLista.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.listaSerwerowLista.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ServerName,
            this.PoziomTrudnosci,
            this.LiczbaGraczy});
            this.listaSerwerowLista.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listaSerwerowLista.FullRowSelect = true;
            this.listaSerwerowLista.HideSelection = false;
            this.listaSerwerowLista.Location = new System.Drawing.Point(319, 105);
            this.listaSerwerowLista.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listaSerwerowLista.Name = "listaSerwerowLista";
            this.listaSerwerowLista.Size = new System.Drawing.Size(671, 335);
            this.listaSerwerowLista.TabIndex = 2;
            this.listaSerwerowLista.UseCompatibleStateImageBehavior = false;
            this.listaSerwerowLista.View = System.Windows.Forms.View.Details;
            this.listaSerwerowLista.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // ServerName
            // 
            this.ServerName.Text = "Nazwa ";
            this.ServerName.Width = 160;
            // 
            // PoziomTrudnosci
            // 
            this.PoziomTrudnosci.Text = "Poziom Trudności";
            this.PoziomTrudnosci.Width = 160;
            // 
            // LiczbaGraczy
            // 
            this.LiczbaGraczy.Text = "Liczba graczy";
            this.LiczbaGraczy.Width = 160;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(443, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(436, 69);
            this.label1.TabIndex = 3;
            this.label1.Text = "Memory Game";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button3.Location = new System.Drawing.Point(696, 457);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(132, 46);
            this.button3.TabIndex = 4;
            this.button3.Text = "Anuluj";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button4.Location = new System.Drawing.Point(849, 457);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(140, 46);
            this.button4.TabIndex = 5;
            this.button4.Text = "Dołącz";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(27, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(195, 25);
            this.label3.TabIndex = 7;
            this.label3.Text = "Nazwa użytkownika: ";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // NazwaGracza
            // 
            this.NazwaGracza.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NazwaGracza.Location = new System.Drawing.Point(32, 138);
            this.NazwaGracza.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NazwaGracza.Multiline = true;
            this.NazwaGracza.Name = "NazwaGracza";
            this.NazwaGracza.Size = new System.Drawing.Size(266, 36);
            this.NazwaGracza.TabIndex = 8;
            this.NazwaGracza.TextChanged += new System.EventHandler(this.NazwaGracza_TextChanged);
            // 
            // MenuWyboru
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1048, 514);
            this.Controls.Add(this.NazwaGracza);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listaSerwerowLista);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "MenuWyboru";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Memo";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListView listaSerwerowLista;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ColumnHeader ServerName;
        private System.Windows.Forms.ColumnHeader PoziomTrudnosci;
        private System.Windows.Forms.ColumnHeader LiczbaGraczy;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NazwaGracza;
    }
}

