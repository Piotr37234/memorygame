﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Memo
{
    public class Serwer
    {
        List<string> mapa = new List<string>()
          { "A", "AG", "A", "A",
            "AG", "AR", "AR", "A",
            "A", "AG", "AG", "A",
            "AR", "R", "AR", "A" };

        List<string> mapa2 = new List<string>()
        { "C", "P", "S", "J", "T", "S", "P", "WI",
          "P", "J", "WI", "AR", "S", "C", "T", "AR",
          "C", "C", "T", "AR", "G", "P", "WI", "G",
          "S", "J", "WI", "T", "S", "AR", "C", "G",
          "G", "P", "C", "T", "P", "S", "J", "G",
          "J", "WI", "T", "J", "J", "WI", "P", "G",
          "AR", "C", "S", "WI", "AR", "S", "G", "AR",
          "J", "T", "G", "AR", "P", "C", "T", "WI" };

        List<string> mapa3 = new List<string>()
        { "WI", "PO", "M", "JA", "P", "WI", "MA", "JA", "A", "P",
          "MA", "B2", "MA", "B2", "A", "BA", "GR", "WI", "M", "BA",
          "M", "P", "GR", "JA", "P", "M", "PO", "B2", "BA", "JA",
          "JA", "WI", "MA", "GR", "A", "B2", "B2", "P", "MA", "GR",
          "P", "A", "PO", "M", "WI", "JA", "B2", "PO", "BA", "MA",
          "M", "JA", "GR", "PO", "A", "P", "M", "MA", "BA", "B2",
          "A", "PO", "WI", "P", "PO", "GR", "WI", "BA", "MA", "WI",
          "PO", "B2", "GR", "A", "JA", "M", "PO", "JA", "BA", "A",
          "B2", "P", "M", "BA", "GR", "WI", "BA", "A", "B2", "GR",
          "M", "PO", "JA", "GR", "A", "MA", "P", "WI", "MA", "BA" };

        private TcpListener tcpLsn;
        public Thread watekSerwera;
      
        //public ArrayList Plansza = new ArrayList();
        //

        public Host host = new Host();
        public DaneGry daneGry = new DaneGry();
        public GraczKlient gracz = new GraczKlient();



        //
        public  int nrGracza = 1;
        public int poziomTrudnosci;
        private int MaxLiczbaGraczy = 400;
        private BinaryFormatter formatowacz = new BinaryFormatter();
        //public delegate void ZdarzenieWyslanieKomunikatuHandler(object dane, ZdarzenieWyslaniaKomunikatu e);
        // public event ZdarzenieWyslanieKomunikatuHandler WyslanoKomunikat; //event wysyłany w razie nadejścia komunikatu

        public void startGame()
        {
            daneGry.start = true;          
        }

        public void endGame()
        {
            daneGry.start = false;
        }

        public void dezaktywacjaBuutonow()
        {
            foreach (PoleGry foto in daneGry.Plansza)
            {
                foto.Enabled = false;

            }

        }


        public void aktywacjaBuutonow()
        {
            foreach (PoleGry foto in daneGry.Plansza)
            {
                foto.Enabled = true;

            }

        }


        public void setHost(String nazwaSerwera, int maxLiczbaGraczy, int poziomTrudnosci, String nazwaGracza){
            string NazwaHosta = Dns.GetHostName();
            this.poziomTrudnosci = poziomTrudnosci;
            IPHostEntry AdresyIP = Dns.GetHostEntry(NazwaHosta);
            host = new Host();
            host.nazwaSerwera = nazwaSerwera;
            host.maxLiczbaGraczy = maxLiczbaGraczy;
            host.poziomTrudnosci = poziomTrudnosci;
            host.port = 2222;


       
            //string kom = "";


            //for(int i=0;i< AdresyIP.AddressList.Length; i++)
            //{
            //    kom += i+": "+ AdresyIP.AddressList[i].MapToIPv4().ToString()+"\n";
            //}

            //MessageBox.Show("komunikat "+kom);
            host.adres = AdresyIP.AddressList[1].MapToIPv4();
            //foreach(IPAddress a in AdresyIP.AddressList)
            // {
            //     kom += a.MapToIPv4() + "\n";
            // }


            


            host.autor = nazwaGracza;     
            gracz.nazwaGracza = nazwaGracza;
            daneGry.listaGraczy = new Hashtable();
            daneGry.listaGraczy.Add(nrGracza,gracz);
            gracz.nrGracza = nrGracza;
            nrGracza++;

           

        }

        public void clearHost()
        {
            host = new Host();       
        }


        public bool wolnyPort(int port)
        {
            try
            {
                TcpListener tcpListener = new TcpListener(IPAddress.Loopback, port);
                tcpListener.Start();
                tcpListener.Stop();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public Host getHost()
        {
            return host;
        }


        TcpClient klientoSerwer;
        NetworkStream ns;
        BinaryReader binaryReader;
        BinaryWriter binaryWriter;


        public bool utworzHostGry(String nazwaSerwera, int maxLiczbaGraczy, int poziomTrudnosci, String nazwaGracza)
        {
            if (wolnyPort(2222) == false)
            {
                MessageBox.Show("Na tym komputerze jest już utworzony serwer. Port 2222 zajety! ");
                return false;
            }
            else
            {

               
                setHost(nazwaSerwera, maxLiczbaGraczy, poziomTrudnosci, nazwaGracza);
                //ustawOknoSerwera();
                tcpLsn = new TcpListener(host.adres, host.port);
                tcpLsn.Start();
                watekSerwera = new Thread(new ThreadStart(OczekiwaczNaKlientow));
                watekSerwera.Name = "Host 1";
                watekSerwera.IsBackground = false;
                watekSerwera.Start();

                klientoSerwer = new TcpClient(host.adres + "", host.port);
                ns = klientoSerwer.GetStream();
                binaryReader = new BinaryReader(ns);
                binaryWriter = new BinaryWriter(ns);
                bf = new BinaryFormatter();
                binaryWriter.Write("HEJKA NIE PRZEJMUJ SIE TO TYLKO KLIENTOSERWER");





               // MessageBox.Show("OK");

                 trener = new Thread(() => IndywidualnaserwerPersonwalnyDlaKlienta(klientoSerwer, gracz));
                trener.Name = "SPECJALNY";
                 trener.IsBackground = false;
                 trener.Start();




                return true;
            }
           
        }

        //public void ustawOknoSerwera()
        //{
        //    DaneStale.oknoSerwera.setAtributes(host, daneGry);
        //}

        //public TcpClient startKlient(string adres, int port)
        //{
        //    GraczKlient kli = new GraczKlient();
        //     IPAddress hostadd = IPAddress.Parse(adres);
        //   IPEndPoint Punkt = new IPEndPoint(hostadd, port);
        //    kli.laczeTCP = new TcpClient();

        //    try
        //     {
        //         kli.laczeTCP.Connect(Punkt);
        //         if (kli.laczeTCP.Client.Connected)
        //         {
        //             //kli.watekGracza = new Thread(new ParameterizedThreadStart(watekCzytajZSocketa));
        //             // kli.watekGracza.Name = "Wątek kllienta czytający z socketa id: 0";
        //             paczkaPostepuGry.listaGraczy.Add(0L, kli);
        //           // kli.watekGracza.Start(0L);
        //         }

        //     }
        //     catch (Exception e1)
        //     {
        //    DaneStale.menuWyboru.setTextTest("tu jest blad");

        //     }
        //     return kli.laczeTCP;
        // }


        // public static long getIdPolaczenia()
        // {
        //     return IdPolaczenia;
        //  }

        BinaryFormatter bf = new BinaryFormatter();
        
        Thread trener;
        int ruch = -1;
        int liczbaGraczy;
        Hashtable hashTableStrumieni = new Hashtable();
        //List<NetworkStream> tablicaStrumieni = new List<NetworkStream>();
        public void IndywidualnaserwerPersonwalnyDlaKlienta(TcpClient tcpClient,GraczKlient graczKlient)
        {
            NetworkStream ns = tcpClient.GetStream();
            BinaryReader binaryReader = new BinaryReader(ns);
            BinaryWriter binaryWriter = new BinaryWriter(ns);

            while (!daneGry.start) ;
            liczbaGraczy = daneGry.listaGraczy.Count;
            ruch = liczbaGraczy;
            bf.Serialize(ns, new Ruch(ruch));

            while (true)
            {

              //  MessageBox.Show("Jakby co czekam na paczke serwer " + graczKlient.nazwaGracza);
                Paczka paka = (Paczka)bf.Deserialize(ns);
                paka.nrGraczaKtorySieRuszyl = graczKlient.nrGracza;
            //    MessageBox.Show("Odebralem pake serwer " + graczKlient.nazwaGracza);
                //if (paka.nrGraczaKtorySieRuszyl != graczKlient.nrGracza)
               // {
               //     MessageBox.Show("ale... to paczka nie do mnie " + graczKlient.nazwaGracza);
               // }
               // else {
                    daneGry.wykonajPaczke(paka);
                    wyslijDoWszystkichKlientow(paka);

                int tmp = ruch - 1;
                if (tmp > 0)
                 {
                     ruch = ruch - 1;
                 }
                else
                {
                    ruch = liczbaGraczy;
                }

                wyslijDoKlienta(ruch, new Ruch(ruch));

               //     MessageBox.Show("Wykonalem pake " + graczKlient.nazwaGracza);
               //  }


                //  if (paka.nrGraczaKtorySieRuszyl == graczKlient.nrGracza)
                //{
                //int tmp = ruch - 1;
                //if (tmp >= 0)
                // {
                //     ruch = ruch - 1;
                // }
                // bf.Serialize(ns, new Ruch(ruch));
                // }


                //  MessageBox.Show("TRENER: " + graczKlient.nazwaGracza + "czekam na start");
                //   MessageBox.Show("TRENER "+ graczKlient.nazwaGracza+": czekam na 13");



                //string kom = "";
                //foreach(string kod in daneGry2.paczka.mapa)
                //{
                //    kom += " " + kod;
                //}


                //MessageBox.Show(kom);

                //DaneGry rezultatRuchu = (DaneGry)bf.Deserialize(ns);




                //MessageBox.Show("TRENER: wysylam pake");
                //bf.Serialize(ns, paczkaPostepuGry);
                //bf.Serialize(ns, daneGry.paczka);
                //MessageBox.Show("TRENER: wyslalem paczke na adresi ip" + graczKlient.ip + " i port" + host.port);

            }           
        }


        public void OczekiwaczNaKlientow()
        {
            try
            {
                while (true)
                {

                    //if (paczkaPostepuGry.start)
                    //{                      
                        //sle do wszystkich
                      //  wyslijDoWszyskichKlientow(paczkaPostepuGry);
                    //}


                    TcpClient tcpKlient = tcpLsn.AcceptTcpClientAbortable();
                    //MessageBox.Show("Cos sie polaczylo! ");
                    string ipadress = ((IPEndPoint)tcpKlient.Client.RemoteEndPoint).Address.ToString();
                    NetworkStream ns = tcpKlient.GetStream();
                    BinaryReader binaryReader = new BinaryReader(ns);
                    BinaryWriter binaryWriter = new BinaryWriter(ns);
                    

                    String polecenie = binaryReader.ReadString();

                    //sle do wszystkich
                    //wyslijDoWszyskichKlientow(paczkaPostepuGry);


                    if (polecenie == "CHCE INFORMACJI OD CIEBIE")
                    {
                        bf.Serialize(ns, host);
                    }
                    else if (polecenie == "JESTEM GRACZEM I CHCE SIE DOLACZYC")
                    {
                        GraczKlient graczKlient = (GraczKlient)bf.Deserialize(ns);
                        graczKlient.ip = ipadress;
                        //MessageBox.Show("adres klienta:" + graczKlient.ip);                       
                        graczKlient.nrGracza = nrGracza;
                        daneGry.listaGraczy.Add(nrGracza, graczKlient);
                        nrGracza++;
                        //MessageBox.Show(" szczesliwy numerek" + graczKlient.nrGracza);
                        DaneStale.oknoSerwera.OdswierzListeGraczy();
                        bf.Serialize(ns, daneGry);

                        trener = new Thread(() => IndywidualnaserwerPersonwalnyDlaKlienta(tcpKlient,graczKlient));
                        trener.Name = "SER2";
                        trener.IsBackground = false;                      
                        trener.Start();
                        hashTableStrumieni.Add(graczKlient.nrGracza, ns);
                    }       
                    else if(polecenie == "HEJKA NIE PRZEJMUJ SIE TO TYLKO KLIENTOSERWER")
                    {

                        //if (poziomTrudnosci == 0)
                        //{
                           


                        //}
                        //else if (poziomTrudnosci == 1)
                        //{
                           

                     


                        //}
                        //else
                        //{
                           
                        //}




                        if (poziomTrudnosci == 0)
                        {
                       //     MessageBox.Show("TU");
                                foreach (string kod in daneGry.paczka.mapa)
                                {
                                    MessageBox.Show(kod);
                                }
                            daneGry.width = 150;
                            daneGry.height = 150;
                            daneGry.rzedow = 4;
                            daneGry.ladujMape(mapa);
                            daneGry.generujMapeIUstawLogike(ns);
                        }
                        else if(poziomTrudnosci == 1)
                        {
                            daneGry.width = 100;
                            daneGry.height = 100;
                            daneGry.rzedow = 8;
                            daneGry.ladujMape(mapa2);
                            daneGry.generujMapeIUstawLogike(ns);
                        }
                        else
                        {
                            daneGry.width = 75;
                            daneGry.height = 75;
                            daneGry.rzedow = 10;
                            daneGry.ladujMape(mapa3);
                            daneGry.generujMapeIUstawLogike(ns);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Nieznana reakcja");

                    }


                    //lock (listaGraczy)
                    //{
                    //    listaGraczy.Add(nrGracza,gracz);
                    //     nrGracza++;
                    // }
                }
            }
            catch (Exception ex)
            {

             //   MessageBox.Show("Blad oczekiwaczNaKlientow! " + ex.StackTrace);
            }
        }


        //public void wyslijDoWszyskichKlientow(Paczka paczka)
        //{
        //    NetworkStream ns;
        //    BinaryReader binaryReader;
        //    BinaryWriter binaryWriter;
        //    TcpClient pol;
        //    MessageBox.Show("Wysulam do wszyskich klientow");
        //    foreach (DictionaryEntry encja in paczkaPostepuGry.listaGraczy)
        //    {
                
        //        String ip = ((GraczKlient)(encja.Value)).ip;
        //        pol = new TcpClient(ip, host.port);
        //        ns = pol.GetStream();
        //        binaryReader = new BinaryReader(ns);
        //        binaryWriter = new BinaryWriter(ns);
        //        bf.Serialize(ns, paczka);
        //        MessageBox.Show("wyslalem paczke aktualizacyjna do klienta na adresi ip"+ip+" i port"+host.port);       
        //    }
        //}

        public void wyslijDoWszystkichKlientow(Paczka paczka)
        {
           // MessageBox.Show("Wysylam do wszystkich klientow");
            foreach (DictionaryEntry encja in hashTableStrumieni)
            {

                if (((int)encja.Key) == 1)
                {
                    continue;
                }else if(((int)encja.Key) == paczka.nrGraczaKtorySieRuszyl)
                {
                   // MessageBox.Show("ODWAL SIE KLIENCIE " + (int)(encja.Key)+" TY NIE DOSTAJESZ");
                    continue;
                }
                else {
                    NetworkStream ns = ((NetworkStream)(encja.Value));

                    bf.Serialize(ns, paczka);
                   // MessageBox.Show("wyslalem paczke aktualizacyjna do klienta "+ (int)(encja.Key));
                }
            }
        }

        public void wyslijDoKlienta(int nr,Paczka paczka)
        {
                    NetworkStream ns = ((NetworkStream)hashTableStrumieni[nr]);
                    bf.Serialize(ns, paczka);
                   // MessageBox.Show("wyslalem paczke do klienta");  
        }

        public void wyslijDoKlienta(int nr, Ruch ruch)
        {
          //  MessageBox.Show("nowy szczesciliwiec to " + nr);
            if (nr == 1)
            {
                DaneStale.oknoSerwera.OdswierzListeGraczy();
                aktywacjaBuutonow();
                MessageBox.Show("Twoj ruch " + gracz.nazwaGracza);
            }
            else
            {
                NetworkStream ns = ((NetworkStream)hashTableStrumieni[nr]);
                bf.Serialize(ns, ruch);
              //  MessageBox.Show("wyslalem ruch do klienta");
            }
        }

        //public void dodawaczGraczy()
        //{
        //    try
        //    {
        //        while (true)
        //        {
        //            GraczKlient gracz = new GraczKlient();
        //            gracz.laczeTCP = tcpLsn.AcceptTcpClientAbortable();
        //            NetworkStream ns = gracz.laczeTCP.GetStream();

        //            MessageBox.Show("Cos sie polaczylo! ");
        //            //gracz.czytanie = new BinaryReader(ns);


        //            //lock (listaGraczy)
        //            //{
        //            //    listaGraczy.Add(nrGracza,gracz);
        //            //     nrGracza++;
        //            // }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Blad oczekiwaczNaKlientow! ");
        //    }
        //}



        public void zakonczSerwer()
        {
            lock (daneGry.listaGraczy)
            {
               //  foreach (GraczKlient kli in listaGraczy.Values)
               //  {
               //      kli.tcpKlient.Client.Disconnect(false);
               //      kli.tcpKlient.Close();
               //      kli.watekGracza.Abort();
                //  }
                daneGry.listaGraczy.Clear();
                clearHost();
                if (watekSerwera != null)
                    watekSerwera.Abort();
                if (tcpLsn != null)
                    tcpLsn.Server.Close();
            }
        }


        //public void watekCzytajZSocketa(object id)
        //{

        //    long realId = (long)id;
        //    TcpClient tcpclient = ((GraczKlient)paczkaPostepuGry.listaGraczy[realId]).laczeTCP;
        //    while (true)
        //    {
        //        if (tcpclient.Connected)
        //        {
        //            try
        //            {

        //                Komunikat odebranyKomunikat = (Komunikat)formatowacz.Deserialize(tcpclient.GetStream());

                        
        //                ZdarzenieWyslaniaKomunikatu zdarzenieWyslaniaKomunikatu = new ZdarzenieWyslaniaKomunikatu();
        //                zdarzenieWyslaniaKomunikatu.komunikat = odebranyKomunikat;
        //                DaneStale.oknoSerwera.setTextTest("fgdfg");

        //                if (WyslanoKomunikat != null)
        //                {
        //                    WyslanoKomunikat(this, zdarzenieWyslaniaKomunikatu);
        //                }

        //                DaneStale.oknoSerwera.setTextTest(odebranyKomunikat.status+"fgdfg");

        //                if (odebranyKomunikat.status == 2)
        //                {

        //                    formatowacz.Serialize(tcpclient.GetStream(), host);
        //                    DaneStale.oknoSerwera.setTextTest("wyslano");
                            
        //                }



        //            }
        //            catch (SerializationException)
        //            {
        //                break;
        //            }
        //            catch (Exception)
        //            {
        //                if (!tcpclient.Connected)
        //                {
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    lock (paczkaPostepuGry.listaGraczy)
        //    {
        //        paczkaPostepuGry.listaGraczy.Remove(realId);
        //    }
        //    //PolaczenieZerwaneEventArgs arg2 = new PolaczenieZerwaneEventArgs(realId);
        //    //if (PolaczenieZerwane != null)
        //    //{
        //    //     PolaczenieZerwane(this, arg2); //zasygnalizuj zerwane połączenie
        //    // }
        //}
    }


}
