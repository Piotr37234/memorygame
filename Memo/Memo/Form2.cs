﻿using Memo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestAplikacjaOkienkowa
{
    public partial class Form2 : Form
    {
        Serwer serwer = new Serwer();
        string zalozyciel;
        public Form2(string zalozyciel)
        {
            InitializeComponent();
            this.zalozyciel = zalozyciel;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string nazwaSerwera = NazwaSerweraPole.Text;
            int maxLiczbaGraczy = Int32.Parse(MaxLiczbaGraczyPole.Text);       
            int poziomTrudnosci;
            if (PoziomTrudnosciRozwijanaLista.Text == "latwy")
            {
                poziomTrudnosci = 0;
            }
            else if(PoziomTrudnosciRozwijanaLista.Text == "sredni")
            {
                poziomTrudnosci = 1;
            }
            else
            {
                poziomTrudnosci = 2;
            }
           // this.Close();
            serwer.utworzHostGry(nazwaSerwera, maxLiczbaGraczy, poziomTrudnosci,zalozyciel);
            Form3 form3 = new Form3();
            form3.Show();

        }

       

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
