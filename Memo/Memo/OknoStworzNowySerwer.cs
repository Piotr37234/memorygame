﻿using Memo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestAplikacjaOkienkowa
{
    public partial class OknoStworzNowySerwer : Form
    {
        
        string zalozyciel;
        public OknoStworzNowySerwer(string zalozyciel)
        {
            InitializeComponent();
            this.zalozyciel = zalozyciel;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string nazwaSerwera = NazwaSerweraPole.Text;
            int maxLiczbaGraczy = Int32.Parse(MaxLiczbaGraczyPole.Text);       
            int poziomTrudnosci;


            string text = listBox1.GetItemText(listBox1.SelectedItem.ToString());
            //MessageBox.Show(text);

            if (listBox1.Text.Equals("latwy"))
            {            
                poziomTrudnosci = 0;
            }
            else if(listBox1.Text.Equals("sredni"))
            {
                poziomTrudnosci = 1;
            }
            else{
                poziomTrudnosci = 2;
            }

            DaneStale.oknoStworzNowySerwer.Hide();
            DaneStale.menuWyboru.Hide();     
            DaneStale.serwer.utworzHostGry(nazwaSerwera, maxLiczbaGraczy, poziomTrudnosci,zalozyciel);

            DaneStale.oknoSerwera = new OknoSerwera();
            
            DaneStale.oknoSerwera.Show();

        }



        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void OknoStworzNowySerwer_Load(object sender, EventArgs e)
        {

        }

        private void NazwaSerweraPole_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void MaxLiczbaGraczyPole_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(MaxLiczbaGraczyPole.Text, "[^0-9]"))
            {
                MessageBox.Show("Niedozwolone znaki, proszę podać liczbę graczy.");
                MaxLiczbaGraczyPole.Text = MaxLiczbaGraczyPole.Text.Remove(MaxLiczbaGraczyPole.Text.Length - 1);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
